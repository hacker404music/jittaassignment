const stock = require('./stock.json')
getminmax(stock).then((result)=>{
    console.dir(result[0]);
    console.dir(result[1])
    console.dir(result[2]);
})
buyandsell('2021-01-06','2021-01-11').then((result)=>{
    
    console.dir(result[0]);
    console.dir(result[1]);
    console.dir(result[2]);
})
async function getminmax (data) {
    const _data = [];
    const stockprice = [];
    for (let stock in data) {
      const detail = data[stock];
      const closePrice = detail.closePrice;
        stockprice.push(closePrice);
    }
    const lowest = Math.min(...stockprice);
    const highest = Math.max(...stockprice);
    _data.push({"lowestpriceday":stock.filter(__data => __data.closePrice == lowest)}
    ,{"highestpriceday":stock.filter(__data => __data.closePrice == highest)},
    {"profit":highest-lowest})
    return _data;
  };
  async function buyandsell (buydate,selldate) {
    const _data = [];
    let buy = stock.filter(__data => __data.date == buydate);
    let sell = stock.filter(__data => __data.date == selldate);
    _data.push({"buyday":buy}
    ,{"sellday":sell},{"profit":sell[0]['closePrice']-buy[0]['closePrice']})
    return _data;
  };